cmake_minimum_required(VERSION 3.7.2)
project(BT-DEPS)
include(ExternalProject)
macro(SET_OPTION option value)
  set(${option} ${value} CACHE INTERNAL "" FORCE)
endmacro()

set(FIND_PACKAGE_SCRIPT_PATH ${CMAKE_BINARY_DIR}/packages)
set(INSTALL_PATH ${CMAKE_SOURCE_DIR}/all_dependencies)

set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR}/bin)
set(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR}/libs)
set(INCLUDE_OUTPUT_PATH ${CMAKE_BINARY_DIR}/include)

SET_OPTION(CMAKE_INSTALL_PREFIX ${INSTALL_PATH})
SET_OPTION(CMAKE_INSTALL_INCLUDEDIR ${INSTALL_PATH}/include)

if (APPLE OR WIN32)
# add_subdirectory(./cegui-deps)
ExternalProject_Add(
    cegui-deps
    SOURCE_DIR ${CMAKE_SOURCE_DIR}/cegui-deps
    BINARY_DIR ${CMAKE_BINARY_DIR}/cegui-deps
    CMAKE_ARGS
    ${GLOBAL_DEFAULT_ARGS}
    ${GLOBAL_THIRDPARTY_LIB_ARGS}
    -DFIND_PACKAGE_SCRIPT_PATH=${FIND_PACKAGE_SCRIPT_PATH}
    UPDATE_COMMAND ""
    BUILD_ALWAYS ON
    INSTALL_DIR ${INSTALL_PATH}
    )

ExternalProject_Add_Step(
        cegui-deps after_install
        COMMAND cmake -DCEGUI_SOURCE_DIR=${CMAKE_SOURCE_DIR}/cegui -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH} -P ${CMAKE_SOURCE_DIR}/cmake/ceguiDeps.cmake
        DEPENDEES mkdir update patch download configure build install)
endif()
# add_subdirectory(lua)
ExternalProject_Add(
    lua
    SOURCE_DIR ${CMAKE_SOURCE_DIR}/lua
    BINARY_DIR ${CMAKE_BINARY_DIR}/lua
    CMAKE_ARGS
    ${GLOBAL_DEFAULT_ARGS}
    ${GLOBAL_THIRDPARTY_LIB_ARGS}
    -DFIND_PACKAGE_SCRIPT_PATH=${FIND_PACKAGE_SCRIPT_PATH}
    -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH}
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    UPDATE_COMMAND ""
    BUILD_ALWAYS ON
    INSTALL_DIR ${INSTALL_PATH}
    )

# SET_OPTION(IRRLICHT_BUILD_EXAMPLES OFF)
# SET_OPTION(IRRLICHT_BUILD_TOOLS OFF)
# SET_OPTION(IRRLICHT_INSTALL_MEDIA_FILES OFF)
# SET_OPTION(IRRLICHT_INSTALL_EXAMPLE_SOURCE OFF)
# add_subdirectory(Irrlicht_extended)
ExternalProject_Add(
    Irrlicht
    SOURCE_DIR ${CMAKE_SOURCE_DIR}/Irrlicht_extended
    BINARY_DIR ${CMAKE_BINARY_DIR}/Irrlicht_extended
    CMAKE_ARGS
    ${GLOBAL_DEFAULT_ARGS}
    ${GLOBAL_THIRDPARTY_LIB_ARGS}
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    -DFIND_PACKAGE_SCRIPT_PATH=${FIND_PACKAGE_SCRIPT_PATH}
    -DIRRLICHT_BUILD_EXAMPLES:BOOL=OFF
    -DIRRLICHT_BUILD_TOOLS:BOOL=OFF
    -DIRRLICHT_INSTALL_MEDIA_FILES:BOOL=OFF
    -DIRRLICHT_INSTALL_EXAMPLE_SOURCE:BOOL=OFF
    -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH}
    UPDATE_COMMAND ""
    BUILD_ALWAYS ON
    INSTALL_DIR ${INSTALL_PATH}
    )
    

#build toluapp
#adding special the position independent code flag
# SET_OPTION(CMAKE_C_FLAGS "-fpic")
# SET_OPTION(CMAKE_CXX_FLAGS "-fpic")
# add_subdirectory(toluapp)

set(LUA_LIB_PATH ${CMAKE_BINARY_DIR}/lua/lib/Release)
set(LUA_LIB_PATH_DBG ${CMAKE_BINARY_DIR}/lua/lib/Debug)
if(UNIX)
    set(LUA_LIB_PATH ${CMAKE_BINARY_DIR}/lua/lib)
    set(LUA_LIB_PATH_DBG ${CMAKE_BINARY_DIR}/lua/lib)
endif()

ExternalProject_Add(
    toluapp
    SOURCE_DIR ${CMAKE_SOURCE_DIR}/toluapp
    BINARY_DIR ${CMAKE_BINARY_DIR}/toluapp
    DEPENDS lua
    CMAKE_ARGS
    ${GLOBAL_DEFAULT_ARGS}
    ${GLOBAL_THIRDPARTY_LIB_ARGS}
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    -DFIND_PACKAGE_SCRIPT_PATH=${FIND_PACKAGE_SCRIPT_PATH}
    -DCMAKE_C_FLAGS=${CMAKE_C_FLAGS} -fpic
    -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS} -fpic
    -DLUA_INCLUDE_DIR=${CMAKE_SOURCE_DIR}/lua/src#${CMAKE_SOURCE_DIR}/lua/include
    -DLUA_SEARCH_PATH=${LUA_LIB_PATH}
    -DLUA_SEARCH_PATH_DBG=${LUA_LIB_PATH_DBG}
    -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH}
    UPDATE_COMMAND ""
    BUILD_ALWAYS ON
    INSTALL_DIR ${INSTALL_PATH}
    )

# SET_OPTION(CEGUI_BUILD_RENDERER_IRRLICHT ON)
# SET_OPTION(IRRLICHT_H_PATH "${CMAKE_CURRENT_SOURCE_DIR}/Irrlicht_extended/include")
# SET_OPTION(TOLUAPP_H_PATH "${CMAKE_CURRENT_SOURCE_DIR}/toluapp/include")
# SET_OPTION(CEGUI_BUILD_LUA_MODULE ON)
# SET_OPTION(TOLUAPP_LIB TOLUAPP)
# SET_OPTION(LUA_H_PATH "${CMAKE_CURRENT_SOURCE_DIR}/lua/include")
# SET_OPTION(LUA_LIB LUA_53)
# SET_OPTION(LUA_MATH_LIB)
# SET_OPTION(CEGUI_SAMPLES_ENABLED OFF)
# SET_OPTION(CEGUI_BUILD_LUA_MODULE ON)
# SET_OPTION(CEGUI_BUILD_LUA_GENERATOR ON)

# find_library(IRRLICHT_LIB
#             NAMES libIrrlicht Irrlicht
#             HINTS ${CMAKE_BINARY_DIR}/Irrlicht_extended
#             )

# find_library(LIBLUA53 
#             NAMES lua53 liblua53
#             HINTS ${LUA_LIB_PATH}
#             )

# find_library(LIBTOLUAPP
#             NAMES libtoluapp libtoluapp toluapp toluapp
#             HINTS ${CMAKE_BINARY_DIR}/toluapp/lib
#             )

if(UNIX)
set(CEGUI_DEPENDENCY lua toluapp Irrlicht)
set(IRRLICHT_LIB ${CMAKE_BINARY_DIR}/Irrlicht_extended/libIrrlicht.a)
set(IRRLICHT_LIB_DBG ${CMAKE_BINARY_DIR}/Irrlicht_extended/libIrrlicht_d.a)
set(LIBLUA53 ${LUA_LIB_PATH}/liblua53.so)
set(LIBLUA53_DBG ${CMAKE_BINARY_DIR}/lua/lib/liblua53_d.so)
set(LIBTOLUAPP ${CMAKE_BINARY_DIR}/toluapp/lib/libtoluapp.so)
set(LIBTOLUAPP_DBG ${CMAKE_BINARY_DIR}/toluapp/lib/libtoluapp_d.so)
endif()

if(MSVC)
set(CEGUI_DEPENDENCY cegui-deps lua toluapp Irrlicht)
set(IRRLICHT_LIB ${CMAKE_BINARY_DIR}/Irrlicht_extended/Release/Irrlicht.lib)
set(IRRLICHT_LIB_DBG ${CMAKE_BINARY_DIR}/Irrlicht_extended/Debug/Irrlicht_d.lib)
set(LIBLUA53 ${LUA_LIB_PATH}/lua53.lib)
set(LIBLUA53_DBG ${CMAKE_BINARY_DIR}/lua/lib/Debug/lua53-d.lib)
set(LIBTOLUAPP ${CMAKE_BINARY_DIR}/toluapp/lib/Release/toluapp.lib)
set(LIBTOLUAPP_DBG ${CMAKE_BINARY_DIR}/toluapp/lib/Debug/toluapp_d.lib)
endif()


if (CMAKE_BUILD_TYPE STREQUAL "Debug")
set(CEGUI_BUILD_SUFFIX "_d")
else()
set(CEGUI_BUILD_SUFFIX "")
endif()
#add_subdirectory(cegui)
ExternalProject_Add(
    cegui
    SOURCE_DIR ${CMAKE_SOURCE_DIR}/cegui
    BINARY_DIR ${CMAKE_BINARY_DIR}/cegui
    DEPENDS ${CEGUI_DEPENDENCY}
    CMAKE_ARGS
    ${GLOBAL_DEFAULT_ARGS}
    ${GLOBAL_THIRDPARTY_LIB_ARGS}
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    -DCEGUI_BUILD_SUFFIX=${CEGUI_BUILD_SUFFIX}
    -DCEGUI_BUILD_RENDERER_IRRLICHT:BOOL=ON
    -DIRRLICHT_H_PATH=${CMAKE_SOURCE_DIR}/Irrlicht_extended/include
    -DIRRLICHT_LIB=${IRRLICHT_LIB}
    -DIRRLICHT_LIB_DBG=${IRRLICHT_LIB_DBG}
    -DTOLUAPP_H_PATH=${CMAKE_SOURCE_DIR}/toluapp/include
    -DTOLUAPP_LIB=${LIBTOLUAPP}
    -DTOLUAPP_LIB_DBG=${LIBTOLUAPP_DBG}
    -DLUA_H_PATH=${CMAKE_SOURCE_DIR}/lua/src
    -DLUA_SEARCH_PATH=${CMAKE_BINARY_DIR}/lua/lib
    -DLUA_LIB=${LIBLUA53}
    -DLUA_LIB_DBG=${LIBLUA53_DBG}
    -DCEGUI_SAMPLES_ENABLED:BOOL=OFF
    -DCEGUI_BUILD_LUA_MODULE:BOOL=ON
    -DCEGUI_BUILD_PYTHON_MODULES:BOOL=OFF
    -DCEGUI_BUILD_LUA_GENERATOR:BOOL=ON
    -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH}
    UPDATE_COMMAND ""
    BUILD_ALWAYS ON
    INSTALL_DIR ${INSTALL_PATH}
    )
ExternalProject_Add_Step(
        cegui after_install
        COMMAND cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH} -P ${CMAKE_SOURCE_DIR}/cmake/ceguiInstall.cmake
        DEPENDEES mkdir update patch download configure build install
)


ExternalProject_Add(
                    RakNet
                    SOURCE_DIR ${CMAKE_SOURCE_DIR}/RakNet
                    BINARY_DIR ${CMAKE_BINARY_DIR}/raknet
                    CMAKE_ARGS
                    ${GLOBAL_DEFAULT_ARGS}
                    ${GLOBAL_THIRDPARTY_LIB_ARGS}
                    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
                    -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH}
                    UPDATE_COMMAND ""
                    INSTALL_COMMAND ""
                    BUILD_ALWAYS ON
                    INSTALL_DIR ${INSTALL_PATH}
                    )

ExternalProject_Add_Step(
                         RakNet after_install
                         COMMAND cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH} -DMSVC=${MSVC} -DCMAKE_SOURCE_DIR=${CMAKE_SOURCE_DIR} -DCMAKE_BINARY_DIR=${CMAKE_BINARY_DIR} -P ${CMAKE_SOURCE_DIR}/cmake/raknetInstall.cmake
                         DEPENDEES mkdir update patch download configure build
)

ExternalProject_Add(
                    SFML
                    SOURCE_DIR ${CMAKE_SOURCE_DIR}/SFML
                    BINARY_DIR ${CMAKE_BINARY_DIR}/sfml
                    CMAKE_ARGS
                    ${GLOBAL_DEFAULT_ARGS}
                    ${GLOBAL_THIRDPARTY_LIB_ARGS}
                    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
                    -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH}
                    -DSFML_BUILD_NETWORK=OFF
                    -DSFML_BUILD_GRAPHICS=OFF
                    -DSFML_BUILD_WINDOW=OFF
                    -DSFML_BUILD_AUDIO=ON
                    UPDATE_COMMAND ""
                    BUILD_ALWAYS ON
                    INSTALL_DIR ${INSTALL_PATH}
                    )

ExternalProject_Add(
                    sol2
                    SOURCE_DIR ${CMAKE_SOURCE_DIR}/sol2
                    BINARY_DIR ${CMAKE_BINARY_DIR}/sol2
                    CMAKE_ARGS
                    ${GLOBAL_DEFAULT_ARGS}
                    ${GLOBAL_THIRDPARTY_LIB_ARGS}
                    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
                    -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH}
                    UPDATE_COMMAND ""
                    BUILD_ALWAYS ON
                    INSTALL_DIR ${INSTALL_PATH}
                    )

ExternalProject_Add(
                    Catch2
                    SOURCE_DIR ${CMAKE_SOURCE_DIR}/Catch2
                    BINARY_DIR ${CMAKE_BINARY_DIR}/Catch2
                    CMAKE_ARGS
                    ${GLOBAL_DEFAULT_ARGS}
                    ${GLOBAL_THIRDPARTY_LIB_ARGS}
                    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
                    -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH}
                    -DBUILD_TESTING:BOOL=OFF
                    UPDATE_COMMAND ""
                    BUILD_ALWAYS ON
                    INSTALL_DIR ${INSTALL_PATH}
                    )

ExternalProject_Add_Step(
                        Catch2 after_install
                        COMMAND cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH} -P ${CMAKE_SOURCE_DIR}/cmake/catchInstall.cmake
                        DEPENDEES mkdir update patch download configure build install
)
