#!/bin/bash

SUDO=""
[ "x$(whoami)" = "xroot" ] || SUDO="sudo"
$SUDO apt-get update && $SUDO apt-get install -y \
    make \
    cmake \
    curl \
    git \
    wget \
    unzip \
    libsilly-dev \
    libexpat1-dev \
    libfreetype6-dev \
    liblua5.3-dev \
    liblua5.3-0 \
    xserver-xorg-dev \
    x11proto-xf86vidmode-dev \
    libxxf86vm-dev \
    mesa-common-dev \
    libgl1-mesa-dev \
    libglu1-mesa-dev \
    libxext-dev \
    libxcursor-dev \
    libgl1-mesa-glx \
    lcov
if [ "x$?" != "x0" ]; then
    ERROR_PACKAGES=1
    ERROR=1
fi