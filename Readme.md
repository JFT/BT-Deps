# Dependencies of the Battletanks Standalone Project
    
    Main Project: https://gitlab.com/JFT/BT_By_KT
    
## Usage:

Make sure you ran:
- `git submodule init`
- `git submodule update`

Then use Cmake to create the build files. After building there should exist a
`all_dependencies` folder in the top repository folder. This folder contains all libs, binaries and headers for
BT-Standalone.
### Unix:
- `mkdir build`
- `cd build`
- `cmake ..`
- `make`

### Windows(MSVC):
- Create a Folder `build`
- run cmake -> source folder: top level of project, bin folder: the created build folder
- configure/generate a VS2015/2017 project with cmake
- build all in VS
