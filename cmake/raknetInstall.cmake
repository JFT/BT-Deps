file(COPY ${CMAKE_BINARY_DIR}/raknet/include/ DESTINATION ${CMAKE_INSTALL_PREFIX}/include/)

if(UNIX)
    FILE(GLOB ALL_RAKNET_LIBS
         ${CMAKE_BINARY_DIR}/raknet/lib/lib*
        )

    file(COPY ${ALL_RAKNET_LIBS} DESTINATION ${CMAKE_INSTALL_PREFIX}/lib)
endif()

if(MSVC)
    FILE(GLOB ALL_RAKNET_LIBS
         ${CMAKE_BINARY_DIR}/raknet/lib/Release/*
        )

FILE(GLOB ALL_RAKNET_DLL
         ${CMAKE_BINARY_DIR}/raknet/lib/Release/*.dll
        )
    
    file(COPY ${ALL_RAKNET_LIBS} DESTINATION ${CMAKE_INSTALL_PREFIX}/lib/)
    
    file(COPY ${ALL_RAKNET_DLL} DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/)
    
    # if(EXISTS ${CMAKE_BINARY_DIR}/raknet/lib/Debug)
       # file(COPY ${CMAKE_BINARY_DIR}/raknet/lib/Debug DESTINATION ${CMAKE_INSTALL_PREFIX}/lib)
    # endif()
endif()