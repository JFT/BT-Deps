if(NOT EXISTS ${CEGUI_SOURCE_DIR}/dependencies)
    file(COPY ${CMAKE_BINARY_DIR}/cegui-deps/dependencies DESTINATION ${CEGUI_SOURCE_DIR})
endif()

FILE(GLOB ALL_CEGUI_DLLS
         ${CMAKE_BINARY_DIR}/cegui-deps/dependencies/bin/*
    )
FILE(GLOB ALL_CEGUI_LIBS
         ${CMAKE_BINARY_DIR}/cegui-deps/dependencies/lib/dynamic/*
    )

file(COPY ${ALL_CEGUI_DLLS} DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)

file(COPY ${ALL_CEGUI_LIBS} DESTINATION ${CMAKE_INSTALL_PREFIX}/lib)