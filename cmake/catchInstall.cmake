if(EXISTS ${CMAKE_INSTALL_PREFIX}/include/catch)
    file(REMOVE_RECURSE ${CMAKE_INSTALL_PREFIX}/include/catch2)
    file(RENAME ${CMAKE_INSTALL_PREFIX}/include/catch ${CMAKE_INSTALL_PREFIX}/include/catch2)
endif()