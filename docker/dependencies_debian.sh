#!/bin/sh

# get relevant packages
SUDO=""
[ "x$(whoami)" = "xroot" ] || SUDO="sudo"
$SUDO apt-get update && $SUDO apt-get install --no-install-recommends -y \
    make \
    cmake \
    libsilly-dev \
    libexpat1-dev \
    libfreetype6-dev \
    xserver-xorg-dev \
    x11proto-xf86vidmode-dev \
    libxxf86vm-dev \
    mesa-common-dev \
    libgl1-mesa-dev \
    libglu1-mesa-dev \
    libxext-dev \
    libxcursor-dev \
    libgl1-mesa-glx \
    libreadline-dev \
    libopenal-dev \
    libvorbis-dev \
    libflac-dev \
    libxft-dev

